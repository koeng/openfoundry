# OpenFoundry protocols
Welcome to OpenFoundry protocols. This repo will grow in complexity over time, so stay tuned!

# Rules for writing code
1. Use underscores instead of camel case (since most of this is python)
2. Initalize labware first, comment with '# Init labware'
3. Initalize pipette next, comment with '# Init pipette'
4. Initalize protocol next, comment with '# Init protocol'

# Rules for OT2
1. 9,10,11 should have tipracks
2. 6,7,8 should not have talls (deep wells, tipracks, etc)
3. 1,2,3 should not have single addresses
4. Left mount uses 10ul multichannel
5. Right mount uses 300ul multichannel

