# Transformation

## Objective
- Make glycerol stocks of important plasmids
 
## Key results
- Transfer 50ul of 80% glycerol from a trough to 6 standard 96 well plates
- Transfer 100ul of cells from a deepwell 96 well plate to each of the 6 96 well plates
  - The transfer should be 1-to-1 (deepwell A1 goes to standard A1)
  - Only one set of pipette tips should be used when transfering a column to each of the standard plates (the tips are used 6 times then discarded)
- Requires 104 pipette tips
