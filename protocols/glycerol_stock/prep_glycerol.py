from opentrons import labware, instruments, robot

# Init labware
tiprack_1 = labware.load('opentrons-tiprack-300ul', '11')
glycerol = labware.load('trough-1row-25ml', '10') # 50% glycerol required

glycerol_stock_1 = labware.load('96-flat', '7')
glycerol_stock_2 = labware.load('96-flat', '4')
glycerol_stock_3 = labware.load('96-flat', '8')
glycerol_stock_4 = labware.load('96-flat', '5')
glycerol_stock_5 = labware.load('96-flat', '9')
glycerol_stock_6 = labware.load('96-flat', '6')

# Init pipette
pipette = instruments.P300_Multi(
    mount='right',
    aspirate_flow_rate=40,
    dispense_flow_rate=40,
    tipracks=[tiprack_1])

# Fix to keep tips from sticking
robot._driver.set_axis_max_speed({'B': 30, 'C': 30})
pipette._drop_tip_current = 0.7

# Init protocol

# Pipette glycerol
for i in range(0,12):
    pipette.pick_up_tip()
    for glycerol_stock in [glycerol_stock_1,glycerol_stock_2,glycerol_stock_3,glycerol_stock_4,glycerol_stock_5,glycerol_stock_6]:
        pipette.transfer(80, glycerol.cols('1'), glycerol_stock.cols(i), new_tip='never')
    pipette.drop_tip()


