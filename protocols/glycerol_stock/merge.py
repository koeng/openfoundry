from opentrons import labware, instruments, robot

# Init labware
tiprack_1 = labware.load('opentrons-tiprack-300ul', '11')
tiprack_2 = labware.load('opentrons-tiprack-300ul', '10')
glycerol = labware.load('trough-1row-25ml', '10') # 50% glycerol required

source_1 = labware.load('96-deep-well', '7')
source_1 = labware.load('96-deep-well', '7')

glycerol_stock_1 = labware.load('96-flat', '8')
glycerol_stock_2 = labware.load('96-flat', '5')

glycerol_stock_3 = labware.load('96-flat', '1')
glycerol_stock_4 = labware.load('96-flat', '2')

merge_10 = labware.load('96-deep-well', '9')
merge_30 = labware.load('96-deep-well', '6')

# Init pipette
pipette = instruments.P300_Multi(mount='right',tipracks=[tiprack_1,tiprack_2])

# Fix to keep tips from sticking
robot._driver.set_axis_max_speed({'B': 30, 'C': 30})
pipette._drop_tip_current = 0.7

# Init protocol

# Pipette glycerol
for i in range(0,12):
    pipette.pick_up_tip()
    pipette.transfer(75, source_1.cols(i), glycerol_stock_1.cols(i), new_tip='never')
    pipette.transfer(75, source_1.cols(i), glycerol_stock_2.cols(i), new_tip='never')
    pipette.transfer(100, source_1.cols(i), merge_10.cols(i), new_tip='never')
    pipette.transfer(75, source_1.cols(i), merge_30.cols(i), new_tip='never')
    pipette.drop_tip()

for i in range(0,12):
    pipette.pick_up_tip()
    pipette.transfer(75, source_2.cols(i), glycerol_stock_3.cols(i), new_tip='never')
    pipette.transfer(75, source_2.cols(i), glycerol_stock_4.cols(i), new_tip='never')
    pipette.transfer(100, source_2.cols(i), merge_10.cols(i), new_tip='never')
    pipette.transfer(75, source_2.cols(i), merge_30.cols(i), new_tip='never')
    pipette.drop_tip()





