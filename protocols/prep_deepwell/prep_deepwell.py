from opentrons import labware, instruments, robot

# Init labware
tiprack = labware.load('opentrons-tiprack-300ul', '5') 
deep_plate = labware.load('96-deep-well', '9')
lb = labware.load('trough-1row-25ml', '6')

# Init pipette
pipette = instruments.P300_Multi(mount='right', tip_racks=[tiprack])

# Fix to keep tips from sticking
robot._driver.set_axis_max_speed({'B': 30, 'C': 30})
pipette._drop_tip_current = 0.7


# Init protocol
for step in range(0,4):
    pipette.pick_up_tip()
    pipette.transfer(300, lb.cols('1'), deep_plate.cols(), new_tip='never')
    pipette.drop_tip()


