from opentrons import labware, instrument, robot

# Init labware
tipracks = [labware.load('tiprack-10ul', '10'),
        labware.load('tiprack-10ul', '11'),
        labware.load('tiprack-10ul', '9'),
        labware.load('tiprack-10ul', '8')]
agar_plate = labware.create(
    'agar_plate',                    # name of you container
    grid=(12, 8),                    # specify amount of (columns, rows)
    spacing=(9, 9),               # distances (mm) between each (column, row)
    diameter=6.4,                     # diameter (mm) of each well on the plate
    depth=10,                       # depth (mm) of each well on the plate
    volume=20)
plates = [labware.load('agar_plate', '4'),
        labware.load('agar_plate', '5'),
        labware.load('agar_plate', '1'),
        labware.load('agar_plate', '2')]
trough = labware.load('trough-1row-25ml', '6')
transformants = labware.load('96-PCR-flat', '3')

# Init pipette
pipette = instruments.P10_Multi(mount='left', tip_racks=[tipracks])

# Init protocol
num_dilutions = 4
plate_vol = 7.5
dilution_vol = 10

counter = 0
for column in transformants.cols():
    counter = counter%12 # Count to 12 columns, then start over
    target_plate = plates[int(cols_used/12)] # Move to next plate each 12 columns
    for _ in range(num_dilutions):
        target_column = target_plate.cols(counter) # Target plate column
        pipette.pick_up_tips()
        pipette.aspirate(dilution_vol, trough.wells('A1'))
        pipette.dispense(dilution_vol, column)
        pipette.aspirate(plate_vol, column)
        pipette.move_to(target_column.top()) # Move above the plate
        pipette.dispense(dilution_vol-1) # Blow out 
        pipette.move_to(target_column.bottom()) # Poke plate
        pipette.drop_tips()
        counter+=1 # Count to next column

