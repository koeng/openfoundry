from opentrons import labware, instruments, robot
from openfoundry_tree import tree

def transformation(dna:tree.DnaNode,competent_cells:tree.OrganismNode):

# Init labware
tiprack1 = labware.load('tiprack-10ul', '10')
tiprack2 = labware.load('tiprack-10ul', '11')
plasmid_dna = labware.load('96-PCR-flat', '4')
competent_cells = labware.load('96-PCR-flat', '5')

# Init pipette
pipette = instruments.P10_Multi(mount='left', tip_racks=[tiprack1,tiprack2])

# Init protocol
pipette.transfer(1, plasmid_dna, competent_cells)


