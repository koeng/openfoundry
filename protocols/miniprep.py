from opentrons import labware, instruments, robot

# Init labware
tiprack = labware.load('opentrons-tiprack-300ul', '11') 
deep_plate = labware.load('96-deep-well', '6')
lb = labware.load('trough-12row', '5')

# Init pipette
pipette = instruments.P300_Multi(mount='right', tip_racks=[tiprack])

# Init protocol
for step in range(0,4):
    pipette.pick_up_tip()
    pipette.transfer(200, lb.cols('1'), deep_plate, new_tip='never')
    pipette.drop_tip()



